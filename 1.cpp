#include<stdio.h>
void one()
{
	printf(":)\n");
	printf("\n");
	printf("现在是一年级题目：\n");
	printf("操作完成\n\n");
}
void two()
{
	printf(":)\n");
	printf("\n");
	printf("现在是二年级题目：\n");
	printf("操作完成\n");
}
void three()
{
	printf(":)\n");
	printf("\n");
	printf("现在是三年级题目：\n");
	printf("操作完成\n");
}
void mistake()
{
	printf("Error!\n");
	printf("错误操作指令，请重新输入\n");
	printf("\n");
}
void four()
{
	printf("\n");
	printf("帮助信息\n");
	printf("你需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位数的加减法;\n");
	printf("二年级的题目为不超过百位数的乘除法;\n");
	printf("三年级的题目为不超过百位的加减乘除的混合题目.\n\n");
}
int main()
{
	printf("======口算生成器======\n");
	printf("欢迎使用口算生成器：\n\n");
	printf("帮助信息\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级的题目为不超过十位数的加减法；\n");
	printf("二年级的题目为不超过百位数的乘除法：\n");
	printf("三年级的题目为不超过百位数的加减乘除的混合题目.\n\n");
	printf("操作列表：\n");
	printf("1)一年级 2）二年级 3）三年级\n");
	printf("4)帮助 5）退出程序\n");
	printf("请输入操作：");
	int n;
	
	scanf("%d", &n);
	while (1)
	{
		if (n == 5)
		{
			break;
		}
		switch (n)
		{
		case 1: one(); break;
		case 2: two(); break;
		case 3: three(); break;
		case 4: four(); break;
		default: mistake(); break;
		}
		printf("操作列表：\n");
		printf("1)一年级 2）二年级 3）三年级\n");
		printf("4)帮助 5）退出程序\n");
		printf("请输入操作：");
		n = 0;
		scanf("%d", &n);
	}
	printf(":)\n");
	printf("\n");
	printf("程序结束，欢迎下次使用，按任意键结束");
	getchar();
	return 0;
}
